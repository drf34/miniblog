$(function () {
    var APPLICATION_ID = "F94702CB-9623-EA9C-FFA0-41C4B43E0D00",
        SECRET_KEY = "0242BE64-D63D-6E51-FF8E-4B1A9E8E8B00",
        VERSION = "v1";
        
        Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
        
        var dataStore = Backendless.Persistence.of(Posts);
        var post = new Posts ({title:"My First Blog Post", content:"My first blog post content", authorEmail:"drf0946@gmail.com"});
        dataStore.save(post);
        
        var postsCollection = Backendless.Persistence.of(Posts).find();
        
        console.log(postsCollection);
        
        var wrapper = {
            posts: postsCollection.data
        };
        
        Handlebars.registerHelper('format', function (time) {
            return moment(time).format("dddd, MMMM Do YYYY");
        });
        
        var blogScript = $("#blogs-template").html();
        var blogTemplate = Handlebars.compile(blogScript);
        var blogHTML = blogTemplate(wrapper);
        
        $('.main-container').html(blogHTML);
    
        
        
});

function Posts (args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}